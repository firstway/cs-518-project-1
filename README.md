CS-518 projects

BUILD:
    gcc wtc_proc.c [-g -D DEBUG]
    gcc wtc_threads.c -lpthread [-g -D DEBUG]

RUN:
    ./a.out [INPUT_FILE   OUTPUT_FILE]
    INPUT_FILE, default: graph.in
    OUTPUT_FILE, default: graph.out

Biao Li & Chen Cong
