

OUT_DIR1=$1
OUT_DIR2=$2

if [ "X$OUT_DIR1" == "X" ] ;then
    echo "$0 out_put_dir1 out_put_dir2"
    exit 1
fi

if [ "X$OUT_DIR2" == "X" ] ;then
    echo "$0 out_put_dir1 out_put_dir2"
    exit 1
fi


for i in {2..6}; do
    for j in {1..10};do
        let n=$j*10
        echo "diff p${i}_n${n}"
        diff $OUT_DIR1/graph_out.p${i}_n${n} $OUT_DIR2/graph_out.p${i}_n${n}
    done
done
