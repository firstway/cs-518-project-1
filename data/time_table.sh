


OUT_DIR=$1

if [ "X$OUT_DIR" == "X" ] ;then
    echo "$0 out_put_dir"
    exit 1
fi

title="-"
for j in {1..10};do
    let n=$j*10
    title="$title $n"
done
echo $title

for i in {2..6}; do
    #echo "time for p$i"
    timedata="p$i"
    for j in {1..10};do
        let n=$j*10
        #echo "for n${n}"
        t=`tail -n 1 $OUT_DIR/graph_out.p${i}_n${n}`
        timedata="$timedata $t"
    done
    echo $timedata
done

