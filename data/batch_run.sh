

OUT_DIR=$1

if [ "X$OUT_DIR" == "X" ] ;then
    echo "$0 out_put_dir"
    exit 1
fi

mkdir -p $OUT_DIR

for i in {2..6}; do
    for j in {1..10};do
        let n=$j*10
        echo "run p${i}_n${n}"
        ../a.out graph_in.p${i}_n${n} > $OUT_DIR/graph_out.p${i}_n${n}
    done
done
