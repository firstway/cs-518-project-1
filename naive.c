
#include <sched.h>
#include <stdio.h>
#include <stdlib.h> 
#include <assert.h>

static size_t Nbytes = 100;

int get_int(char *buf, int* offset, int len){
    if(len<=0) return -1;
    int begin;
    while(len>0 && !isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    begin = *offset;
    if(len<=0) return -2;
    while(len>0 && isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    buf[*offset] = 0;
    return atoi(buf+begin);
};

int ** gen_2d_array(int n){
    int ** array2 = (int**)malloc (sizeof(int*) * n);
    int i = 0;
    for(i=0; i < n; i++){
        array2[i] = (int*)malloc (sizeof(int) * n );
        int j ;
        for(j=0; j < n; j++){
            array2[i][j] = 0;
        }
    }
    return array2;
};

int** get_input(FILE *f, int* p, int* n){
    int ** array2 = NULL;
    *p = 0;
    *n = 0;
   char *buf = (char *) malloc (Nbytes + 1);
   int bytes_read;
   while( (bytes_read = getline (&buf, &Nbytes, f)) >0){
        buf[bytes_read] = 0;
        /*printf("[%s]\n", buf);*/
        int offset = 0;
        if(*p <=0){
            *p = get_int(buf, &offset, bytes_read);
            if(*p <= 0){
                fprintf(stderr, "Can NOT parse int for p,[%s]\n", buf);
                return NULL;
            }
            //printf("p=%d\n", *p);
        }else if(*n <= 0){
            *n = get_int(buf, &offset, bytes_read);
            if(*n <= 0){
                fprintf(stderr, "Can NOT parse int for n,[%s]\n", buf);
                return NULL;
            }
            assert(n>0 && *n < 100);
            //printf("n=%d\n", *n);
            array2 = gen_2d_array(*n +1);
        }else{
            assert(array2 != NULL);
            int from = get_int(buf, &offset, bytes_read);
            assert(from <= *n);
            bytes_read -= offset;
            int to = get_int(buf, &offset, bytes_read);
            assert(to <= *n);
            array2[from][to] = 1;
            //printf("%d -> %d\n", from, to);
        }
   };

   return array2;
};
/* Sequential Implementation */
int ** TRANSITIVE_CLOSURE_Sequential(int** a2, int n){
    int na = n+1;
    int*** t = (int***)malloc( sizeof(int**) * na );
    int i,j;
    for(i=0; i < na; i++){
        t[i] = gen_2d_array(na);
    };
    /* initialization T(0) */
    for(i=1; i < na; i++){
        for(j=1; j < na; j++){
            if(i == j){
                t[0][i][j] = 1;
            }else{
                t[0][i][j] = a2[i][j];
            }
        }
    }
    int k = 1;
    for(k=1; k < na; k++){
        for(i=1; i < na; i++){
            for(j=1; j < na; j++){
                if(t[k-1][i][j] > 0){
                    t[k][i][j] = 1;
                }else if(t[k-1][i][k] > 0 && t[k-1][k][j] > 0 ){
                    t[k][i][j] = 1;
                }
            }/* end of lopp j*/
        }/* end of lopp i*/
    };/* end of lopp k*/

    return t[n];
};



int main(int argc, char *argv[]){
    int p,n;
    FILE *fin = stdin;
    FILE *fout = stdout;
    if(argc > 1)
    {
        fin = fopen(argv[1], "r");
    }else{
        fin = fopen("graph.in", "r");
    }

    if(argc > 2){
        fout = fopen(argv[2], "w");
    }else{
        fout = fopen("graph.out", "w");
    }
    int** arrays = get_input(fin, &p, &n);
    int i,j;
    
    for(i=1; i < n+1; i++){
        for(j=1; j < n+1; j++){
            printf("input,[%d,%d] = %d\n",i,j,arrays[i][j]);
        }
    }
    printf("*************Sequential Implementation output******************\n");
    int **output = TRANSITIVE_CLOSURE_Sequential(arrays, n);
    for(i=1; i < n+1; i++){
        for(j=1; j < n+1; j++){
            fprintf(fout, "[%d,%d]=%d\n",i,j,output[i][j]);
        }
    }
    /* end of Sequential Implementation*/

}
