



import sys
import random






def gen_edge(n, num_of_e):
    for i in range(num_of_e):
        yield (random.randint(1,n), random.randint(1,n))

def gen_graph(outf, p, n, num_of_e):
    print >>outf, p
    print >>outf, n
    for f,t in gen_edge(n, num_of_e):
        print >>outf, f,' ',t


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print >>sys.stderr, "%s P N [Edge]" % sys.argv[0]
        sys.exit(-1)
    p = int(sys.argv[1])
    n = int(sys.argv[2])
    e  = n*3
    if len(sys.argv) > 3:
        e = int(sys.argv[3])
    gen_graph(sys.stdout, p, n, e)


