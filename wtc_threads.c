
#include <sched.h>
#include <stdio.h>
#include <stdlib.h> 
#include <ctype.h>
#include <assert.h>


#include <pthread.h>

///////////////////////////
static size_t Nbytes = 100;
int get_int(char *buf, int* offset, int len){
    if(len<=0) return -1;
    int begin;
    while(len>0 && !isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    begin = *offset;
    if(len<=0) return -2;
    while(len>0 && isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    buf[*offset] = 0;
    return atoi(buf+begin);
};

int** gen_2d_array(int n){
    int ** array2 = (int**)malloc (sizeof(int*) * n);
    int i = 0;
    for(i=0; i < n; i++){
        array2[i] = (int*)malloc (sizeof(int) * n );
        int j ;
        for(j=0; j < n; j++){
            array2[i][j] = 0;
        }
    }
    return array2;
};

int** get_input(FILE *f, int* p, int* n){
    int ** array2 = NULL;
    *p = 0;
    *n = 0;
   char *buf = (char *) malloc (Nbytes + 1);
   int bytes_read;
   while( (bytes_read = getline (&buf, &Nbytes, f)) >0){
        buf[bytes_read] = 0;
        /*printf("[%s]\n", buf);*/
        int offset = 0;
        if(*p <=0){
            *p = get_int(buf, &offset, bytes_read);
            if(*p <= 0){
                fprintf(stderr, "Can NOT parse int for p,[%s]\n", buf);
                return NULL;
            }
            //printf("p=%d\n", *p);
        }else if(*n <= 0){
            *n = get_int(buf, &offset, bytes_read);
            if(*n <= 0){
                fprintf(stderr, "Can NOT parse int for n,[%s]\n", buf);
                return NULL;
            }
            assert(n>0 && *n < 255);
            //printf("n=%d\n", *n);
            array2 = gen_2d_array(*n +1);
        }else{
            assert(array2 != NULL);
            int from = get_int(buf, &offset, bytes_read);
            assert(from <= *n);
            bytes_read -= offset;
            int to = get_int(buf, &offset, bytes_read);
            assert(to <= *n);
            array2[from][to] = 1;
            //printf("%d -> %d\n", from, to);
        }
   };

   return array2;
};
//\\\\\\\\\\\\\\\\\\\\\\\\\\

struct ttask_queue{
    pthread_mutex_t *mutex;
    pthread_cond_t *cond_item;
    int n;
    int k;
    int i;
};


struct ThreadVar{
    pthread_mutex_t *lock;
    pthread_cond_t *cond_done;
    int done;
    int*** d2;
    struct ttask_queue *q;
};


int add_done(struct ThreadVar* ptv, int add){
   int d =0;
   pthread_mutex_lock(ptv->lock); 
   ptv->done += add;
   d = ptv->done;
   pthread_mutex_unlock(ptv->lock); 
   pthread_cond_signal(ptv->cond_done);
   return d;
};

void set_done(struct ThreadVar* ptv, int d){
   pthread_mutex_lock(ptv->lock); 
   ptv->done = d;
   pthread_mutex_unlock(ptv->lock); 
};

void wait_done(struct ThreadVar* ptv, int except){
   pthread_mutex_lock(ptv->lock); 
   while(ptv->done != except){
        pthread_cond_wait(ptv->cond_done, ptv->lock);
   };
   pthread_mutex_unlock(ptv->lock); 
};

/////// queue

void set_queue(struct ttask_queue *q, int n, int k, int i){
    pthread_mutex_lock(q->mutex); 
    q->n = n;
    q->k = k;
    q->i = i;
    pthread_mutex_unlock(q->mutex); 
    pthread_cond_broadcast(q->cond_item);
};

void get_k_addi_q(struct ttask_queue *q, int* k, int* i){
    pthread_mutex_lock(q->mutex); 
    while(q->i >= q->n){
        pthread_cond_wait(q->cond_item, q->mutex);
    };
    *i = q->i+1;
    q->i = *i;
    *k = q->k;
    pthread_mutex_unlock(q->mutex); 
};

void* worker(void *ptr ){
    struct ThreadVar* tv = (struct ThreadVar*)ptr;
    pthread_t tid = pthread_self();
    int k = -1;
    int i;

    #ifdef DEBUG
    printf("enter thread %u\n", tid);
    #endif
    while(1){
        get_k_addi_q(tv->q, &k, &i);
        if(k > tv->q->n){
            break;
        }
        //printf("in %u thread:k=%d, i=%d\n",tid, k, i);            
        int j;
        for(j=1; j <= tv->q->n; j++){
            int ret = 0;
            if(tv->d2[(k-1)%2][i][j] > 0)
            {
                ret = 1;
            }else if( tv->d2[(k-1)%2][i][k] > 0 && tv->d2[(k-1)%2][k][j] ){
                ret = 1;
            }
            tv->d2[k%2][i][j] = ret;
        }/* end of lopp j*/
        add_done(tv, 1);
    }
    #ifdef DEBUG
    printf("quit thread %u\n", tid);
    #endif
    return NULL;
}; 

void poutput(FILE* fout, int** d2, int n){
    int i,j;
    for(i=1; i <=n ; i++){
        for(j=1; j <=n ; j++){
            if(d2[i][j]) fprintf(fout, "%d %d\n", i, j);
        }
    }
};

int main(int argc, char *argv[]){

    int p=0;
    int n = 0;
    FILE *fin = stdin;
    FILE *fout = stdout;
    if(argc > 1)
    {
        fin = fopen(argv[1], "r");
    }else{
        fin = fopen("graph.in", "r");
    }

    if(argc > 2){
        fout = fopen(argv[2], "w");
    }

    int** arrays = get_input(fin, &p, &n);
    if(p<=0 || n<= 0){
        fprintf(stderr, "error; p=%d, n=%d\n", p, n);
        return -1;
    }
    int i,j;
    #ifdef DEBUG
    for(i=1; i < n+1; i++){
        for(j=1; j < n+1; j++){
            printf("input matrix:[%d,%d] = %d\n",i,j,arrays[i][j]);
        }
    }
    #endif

    // time
    unsigned int start = 0;
    unsigned int stop = 0;
    unsigned int base_overhead = 0;
    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    "RDTSC;"
    "movl %%eax, %1;"
    : "=r"(start), "=r"(stop)
    : /* No input */
    : "eax", "0", "1"
    ); 
    base_overhead = stop - start;

    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(start)
    : /* No input */
    : "eax", "0"
    ); 

    int na = n+1;
    pthread_t* threads = (pthread_t*)malloc(sizeof(pthread_t) * p);

    struct ThreadVar tv;
    pthread_cond_t cond_var_item, cond_var_done;
    pthread_cond_init(&cond_var_item, NULL);
    pthread_cond_init(&cond_var_done, NULL);
    pthread_mutex_t mut1 = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t mut2 = PTHREAD_MUTEX_INITIALIZER;

    //init done
    tv.lock = &mut1;
    tv.cond_done = &cond_var_done;
    set_done(&tv, 0);

    // init queue
    tv.q = (struct ttask_queue*)malloc( sizeof(struct ttask_queue) );
    tv.q->mutex = &mut2;
    tv.q->cond_item = &cond_var_item;
    //set_queue(struct ttask_queue *q, int n, int k, int i)
    set_queue(tv.q, n, 0, n);//set full, (i==n)

    // shared matrix
    tv.d2 = (int ***)malloc( sizeof(int**) * 2);
    tv.d2[0] = gen_2d_array(na);
    tv.d2[1] = gen_2d_array(na);

    int ret = -1;
    //create threads p
    for(i=0; i <p ; i++){
        ret = pthread_create(&threads[i], NULL, worker, (void*)&tv);
        if(ret)
        {
            fprintf(stderr,"Error - pthread_create() return code: %d\n",ret);
            return -2;
        }

    }


    //init T(0)
    for(i=1; i < na; i++){
        for(j=1; j < na; j++){
            if(i == j){
                //t[0][i][j] = 1;
                tv.d2[0][i][j] = 1;
            }else{
                //t[0][i][j] = a2[i][j];
                tv.d2[0][i][j] = arrays[i][j];
            }
        }
    }// now T(0) is OK

    int k;
    for(k=1; k <= n; k++){
        set_done(&tv, 0);
        set_queue(tv.q, n, k, 0);//set task: ( 1 <= k <= n)
        wait_done(&tv, n);
    };
    /// end of time
    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(stop)
    : /* No input */
    : "eax", "0"
    ); 

    printf("%d\n%d\n", p, n);
    poutput(fout, tv.d2[n%2], n);
    fprintf(fout,"%d\n", stop - start - base_overhead ); 

    // joi  all threads
    set_queue(tv.q, n, n+1, 0);//set finish flag: (k>n)
    for(i=0; i <p ; i++){
        pthread_join(threads[i], NULL);
    }
    fclose(fout);
    return 0;
};

