
#include <sched.h>
#include <stdio.h>
#include <stdlib.h> 
#include <ctype.h>
#include <assert.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

static size_t Nbytes = 100;

#define KEY_LOCK    18888
#define KEY_TASK    18899
#define KEY_FINISH  19999
/**********************************/
static struct sembuf sem_wait,sem_signal;

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};



int semaphore_alloc (key_t key, int sem_flags)
{
    return semget (key, 1, sem_flags);
}
/* Deallocate a binary semaphore. All users must have finished their
 * use. Returns -1 on failure. */
int semaphore_dealloc (int semid)
{
    union semun ignored_argument;
    return semctl (semid, 1, IPC_RMID, ignored_argument);
}


int semaphore_init (int semid, int v)
{
    union semun argument;
    unsigned short values[1];
    values[0] = v;
    argument.array = values;
    return semctl (semid, 0, SETALL, argument);
}

int get_sem_val(int semid){
    return  semctl(semid,0,GETVAL);
};
/**********************************/

int get_int(char *buf, int* offset, int len){
    if(len<=0) return -1;
    int begin;
    while(len>0 && !isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    begin = *offset;
    if(len<=0) return -2;
    while(len>0 && isdigit(buf[*offset])){
        len --;
        (*offset) += 1;
    };
    buf[*offset] = 0;
    return atoi(buf+begin);
};

int ** gen_2d_array(int n){
    int ** array2 = (int**)malloc (sizeof(int*) * n);
    int i = 0;
    for(i=0; i < n; i++){
        array2[i] = (int*)malloc (sizeof(int) * n );
        int j ;
        for(j=0; j < n; j++){
            array2[i][j] = 0;
        }
    }
    return array2;
};

int** get_input(FILE *f, int* p, int* n){
    int ** array2 = NULL;
    *p = 0;
    *n = 0;
   char *buf = (char *) malloc (Nbytes + 1);
   int bytes_read;
   while( (bytes_read = getline (&buf, &Nbytes, f)) >0){
        buf[bytes_read] = 0;
        /*printf("[%s]\n", buf);*/
        int offset = 0;
        if(*p <=0){
            *p = get_int(buf, &offset, bytes_read);
            if(*p <= 0){
                fprintf(stderr, "Can NOT parse int for p,[%s]\n", buf);
                return NULL;
            }
            //printf("p=%d\n", *p);
        }else if(*n <= 0){
            *n = get_int(buf, &offset, bytes_read);
            if(*n <= 0){
                fprintf(stderr, "Can NOT parse int for n,[%s]\n", buf);
                return NULL;
            }
            assert(n>0 && *n < 255);
            //printf("n=%d\n", *n);
            array2 = gen_2d_array(*n +1);
        }else{
            assert(array2 != NULL);
            int from = get_int(buf, &offset, bytes_read);
            assert(from <= *n);
            bytes_read -= offset;
            int to = get_int(buf, &offset, bytes_read);
            assert(to <= *n);
            array2[from][to] = 1;
            //printf("%d -> %d\n", from, to);
        }
   };

   return array2;
};


inline char two_dimension_get(const char *a, int dimen, int i, int j){
    return a[dimen*i + j];
}; 
inline void two_dimension_set(char *a, int dimen, int i, int j, char value){
    a[dimen*i + j] = value;
}; 


inline char three_dimension_get(const char *a, int dimen, int k, int i, int j){
    return a[dimen*dimen*(k%2) + dimen*i + j];
}; 
inline void three_dimension_set(char *a, int dimen, int k, int i, int j, char value){
    a[dimen*dimen*(k%2) + dimen*i + j] = value;
}; 



/*****************/

int fetch_i(char *sbuf, int sem_id_lock, int offset){
   int i = -1;
   semop(sem_id_lock,&sem_wait,1); 
   sbuf[offset] += 1;
   i = sbuf[offset];
   semop(sem_id_lock,&sem_signal,1);
   return i;
};

int get_k(char* sbuf,int sem_id_lock,int offset){
   int k = -1;
   semop(sem_id_lock,&sem_wait,1); 
   k = sbuf[offset];
   semop(sem_id_lock,&sem_signal,1);
   return k;
};

void set_ki(char* sbuf,int sem_id_lock,int offset, int k){
   semop(sem_id_lock,&sem_wait,1); 
   sbuf[offset] = k;
   semop(sem_id_lock,&sem_signal,1);
};

void add_k(char* sbuf,int sem_id_lock,int offset){
   semop(sem_id_lock,&sem_wait,1); 
   sbuf[offset] += 1;
   semop(sem_id_lock,&sem_signal,1);
};

int subprocess(int shm_id, int sem_id_lock, int sem_id_task, int sem_id_finish, int n){
    /*do some job in child process*/
    pid_t pid =  getpid();
    #ifdef DEBUG
    printf("child %d enter\n", pid);
    #endif
    char *sbuf = shmat(shm_id,NULL,0);
    int na = n+1;
    int offset = 2*(n+1)*(n+1);
    while(1){
        semop(sem_id_task, &sem_wait, 1);/** wait for task*/
        int k_value = get_k(sbuf, sem_id_lock, offset+1);
        int i_value = fetch_i(sbuf, sem_id_lock, offset+2);
        if(k_value > n){
            break;
        }
        //printf("pid=%d process task for k=%d, i=%d\n",pid, k_value, i_value);
        int j;
        for(j=1; j < na; j++){
            /*if(t[k-1][i][j] > 0){
                t[k][i][j] = 1;
            }else if(t[k-1][i][k] > 0 && t[k-1][k][j] > 0 ){
                t[k][i][j] = 1;
            }*/
            char v = 0;
            if(three_dimension_get(sbuf, na, k_value-1, i_value,  j) > 0){
                v = 1;
            }else if( three_dimension_get(sbuf, na, k_value-1, i_value,  k_value) > 0 && 
                      three_dimension_get(sbuf, na, k_value-1, k_value,  j) > 0
                    ){
                v = 1;
            }
            three_dimension_set(sbuf, na, k_value, i_value,  j, v);
        }/* end of lopp j*/
        /* notify parent that job is done*/
        semop(sem_id_finish, &sem_signal, 1);
    }
    #ifdef DEBUG
    printf("child %d quit\n", pid);
    #endif
    return 0;
};

int get_shm(size_t s){
    int shm_id;
    char* tmp_name = "/tmp/biao_chen_cs518_2014_fall";
    FILE* tmpf = fopen(tmp_name,"w");
    fclose(tmpf);
    key_t key = ftok(tmp_name,0x11+s);
    #ifdef DEBUG
    printf("key=%x\n",key) ;
    #endif
    shm_id = shmget(key, s ,IPC_CREAT|0600); 
    if(shm_id==-1)
    {
        perror("shmget error");
        return -1;
    }
    #ifdef DEBUG
    printf("shm_id=%d\n", shm_id) ;
    #endif
    return shm_id;
};


void poutput(FILE* outf, char *sbuf, int n){
    int na = n+1;
    int i,j;
    for(i=1; i < n+1; i++){
        for(j=1; j < n+1; j++){
            int v = three_dimension_get(sbuf, na, n, i, j); 
            if(v) fprintf(outf, "%d %d\n",i,j);
        }
    }
};

int main(int argc, char *argv[]){
    int p,n;
    FILE *fin = stdin;
    FILE *fout = stdout;
    if(argc > 1)
    {
        fin = fopen(argv[1], "r");
    }else{
        fin = fopen("graph.in", "r");
    }

    if(argc > 2){
        fout = fopen(argv[2], "w");
    }
    
    int** arrays = get_input(fin, &p, &n);
    int i,j;

    // time
    unsigned int start = 0;
    unsigned int stop = 0;
    unsigned int base_overhead = 0;
    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    "RDTSC;"
    "movl %%eax, %1;"
    : "=r"(start), "=r"(stop)
    : /* No input */
    : "eax", "0", "1"
    ); 
    base_overhead = stop - start;

    //////////

    #ifdef DEBUG
    printf("p=%d, n=%d\n", p, n);
    #endif
    int na = n+1;

    sem_wait.sem_num = 0;
    sem_wait.sem_op = -1;
    sem_wait.sem_flg = SEM_UNDO;

    sem_signal.sem_num = 0;
    sem_signal.sem_op = 1;
    sem_signal.sem_flg = SEM_UNDO;

    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(start)
    : /* No input */
    : "eax", "0"
    ); 

    /* prepare the share memory*/
    int shm_id = get_shm(2*na*na+8);
    if(shm_id < 0){
        fprintf(stderr, "get shm error!!!\n");
        return -1;
    }


    /* prepare these semaphores */
    int sem_id_lock = semaphore_alloc(KEY_LOCK, IPC_CREAT|0666);
    int sem_id_task = semaphore_alloc(KEY_TASK, IPC_CREAT|0666);
    int sem_id_finish = semaphore_alloc(KEY_FINISH, IPC_CREAT|0666);

    semaphore_init (sem_id_lock, 1);
    semaphore_init (sem_id_task, 0);
    semaphore_init (sem_id_finish, 0);

    /** process in parallel */
    for(i=0; i < p; i++){
        int childpid = fork();
        if (childpid < 0){
            fprintf(stderr, "forl error!!!\n");
            return -1;
        };
        if (childpid == 0){/* child process */
            return subprocess(shm_id, sem_id_lock, sem_id_task, sem_id_finish, n);
        };
    }

    /** in parent, contrl the whole job */
    char *sbuf = shmat(shm_id,NULL,0);
    //memset();

    /* initialization T(0) */
    for(i=1; i < na; i++){
        for(j=1; j < na; j++){
            if(i == j){
                //t[0][i][j] = 1;
                three_dimension_set(sbuf, na, 0, i, j, 1);
            }else{
                //t[0][i][j] = a2[i][j];
                three_dimension_set(sbuf, na, 0, i, j, arrays[i][j]);
            }
        }
    }// now T(0) is OK

    int offset = 2*(n+1)*(n+1);
    int k ;
    for(k =1; k <= n; k++){
        set_ki(sbuf, sem_id_lock, offset+1, k);//set k
        set_ki(sbuf, sem_id_lock, offset+2, 0);//set i
        for(j=1; j<= n; j++){
            semop(sem_id_task, &sem_signal, 1);
        }
        for(j=1; j<= n; j++){
            semop(sem_id_finish, &sem_wait, 1);
        }
        //printf("in parent, finish k=%d\n",k);
    }
    #ifdef DEBUG
    printf("have finished job, notify children\n");
    #endif
    //notify children to quit
    set_ki(sbuf, sem_id_lock, offset+1, n+1);//set k = N+1
    for(j=0; j< p; j++){
        semop(sem_id_task, &sem_signal, 1);
    }
    /// end of time
    __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(stop)
    : /* No input */
    : "eax", "0"
    ); 
    
    printf("%d\n%d\n", p, n);
    poutput(fout, sbuf, n);
    fprintf(fout,"%d\n", stop - start - base_overhead ); 
    fclose(fout);
    return 0;
};




