
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>




int main(){
    int shm_id,i;
    key_t key = ftok("/tmp",0x11);
    printf("key=%x\n",key) ;

    shm_id = shmget(key,1024,IPC_CREAT|0600); 
    if(shm_id==-1)
    {
        perror("shmget error");
        return -1;
    }
    printf("shm_id=%d\n", shm_id) ;




    int childpid = fork();
    if (childpid < 0){
        fprintf(stderr, "forl error!!!\n");
        return -1;
    };
    if (childpid == 0){/* child process */
        char *sh_buf_child = shmat(shm_id,NULL,0);
        printf("in child\n");
        printf("in child,shm_buf addr=%p\n", sh_buf_child) ;
        for(i=0; i< 20; i++){
            sh_buf_child[i] = 'a'+i;
        }
        printf("last line in child\n");
        return 0;
    };
    /* parent  */
    printf("in parent\n");
    malloc(1024*501);
    //sleep(3); /* sleep for 1 second */
    char *sh_buf = shmat(shm_id,NULL,0);
    printf("in parent, shm_buf addr=%p\n", sh_buf) ;
    for(i=0; i< 25; i++){
        printf("->%d\n",sh_buf[i]);
    }

};


