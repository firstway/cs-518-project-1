

#include <stdio.h>
#include<stdlib.h>
#include<errno.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>

#define MYKEY 16666

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};



int binary_semaphore_allocation (key_t key, int sem_flags)
{
    return semget (key, 1, sem_flags);
}
/* Deallocate a binary semaphore. All users must have finished their
 * use. Returns -1 on failure. */
int binary_semaphore_deallocate (int semid)
{
    union semun ignored_argument;
    return semctl (semid, 1, IPC_RMID, ignored_argument);
}


int binary_semaphore_initialize (int semid, int v)
{
    union semun argument;
    unsigned short values[1];
    values[0] = v;
    argument.array = values;
    return semctl (semid, 0, SETALL, argument);
}

int get_sem_val(int semid){
    return  semctl(semid,0,GETVAL);
};

int main()
{
    struct sembuf sem_wait,sem_signal;

    sem_wait.sem_num = 0;
    sem_wait.sem_op = -1;
    sem_wait.sem_flg = SEM_UNDO;

    sem_signal.sem_num = 0;
    sem_signal.sem_op = 1;
    sem_signal.sem_flg = SEM_UNDO;
    /**********************************/

    int semid;
    semid = binary_semaphore_allocation(MYKEY, IPC_CREAT|0666); 
    printf("semid=%d\n",semid);

    if( binary_semaphore_initialize(semid, 2) < 0)
    {
        perror(NULL);
        fprintf( stderr, "Cannot set semaphore value.\n");
    }
    else
    {
        fprintf(stderr, "Semaphore initialized OK.\n");
        int semval = semctl(semid,0,GETVAL);
        printf("Initialized Semaphore value to %d\n",semval);
    }
    
    
    int childpid = fork();
    if (childpid < 0){
        fprintf(stderr, "forl error!!!\n");
        return -1;
    };
    if (childpid == 0){/* child process */
        sleep(5); /* sleep for 1 second */
        printf("in child\n");
        printf("in child, get_sem_val:%d\n", get_sem_val(semid));
        semop(semid,&sem_signal,1);
        printf("last line in child\n");
        return 0;
    };
    /* parent  */
    printf("in parent\n");
    int status = 0;
    printf("in parent, get_sem_val:%d\n", get_sem_val(semid));
    semop(semid,&sem_wait,1);
    printf("in parent, get_sem_val:%d\n", get_sem_val(semid));
    semop(semid,&sem_wait,1);
    printf("in parent, get_sem_val:%d\n", get_sem_val(semid));
    /*wait(&status); wait for child to exit, and store its status */
    printf("PARENT: Child's exit code is: %d\n", status);
    printf("PARENT: Goodbye!\n");             
    return 0;
};

