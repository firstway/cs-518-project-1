#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <unistd.h>
#include <sys/sem.h> 

#define BUFFER_SIZE 100
#define SEM_KEY 0
#define ROW_SHM_KEY 1
#define TMP_SHM_KEY 2

/* union semun for semaphores */
typedef union {
  int val;
  struct semid_ds* buf;
  unsigned short* array;
} semun;

/* Write message and strerror(errno) to stderr, and exit with a failure */
void die(char* message);

/* Check the condition, if true then die with the message */
void die_if(int condition, char* message);

/* Parse the input text into a matrix represented by an array. */
int* parse_input(char* filename);

/* Return the transitive closure of the given input. */
int* get_transitive_closure(int* input);

/* Output the result onto the screen. */
void output(int* transitive_closure, int time);


int main() {
  unsigned int start = 0;
  unsigned int stop = 0;
  unsigned int base_overhead = 0;
  __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    "RDTSC;"
    "movl %%eax, %1;"
    : "=r"(start), "=r"(stop)
    : /* No input */
    : "eax", "0", "1"
  ); 

  base_overhead = stop - start;

  int* input = parse_input("graph.in");

  __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(start)
    : /* No input */
    : "eax", "0"
  ); 

  int* transitive_closure = get_transitive_closure(input);

  __asm__ __volatile__ (
    "RDTSC;"
    "movl %%eax, %0;"
    : "=r"(stop)
    : /* No input */
    : "eax", "0"
  ); 

  output(transitive_closure, stop - start - base_overhead);

  /* GC */
  free(input);
  free(transitive_closure);
  return EXIT_SUCCESS;
}

void die(char* message) {
  if (errno != 0) {
    fprintf(stderr, "%s (%s)\n", message, strerror(errno));
  }
  else {
    fprintf(stderr, message);
  }
  exit(EXIT_FAILURE);
}

void die_if(int condition, char* message) {
  if (condition != 0) {
    die(message);
  }
}

/* Parse the input file into p, n and the adjacency matrix
 * The argument is the filename of the input file */
int* parse_input(char* filename) {
  FILE* file = fopen(filename, "r");
  die_if(file == NULL, "Empty input file");

  size_t buffer_size = BUFFER_SIZE;
  char* buffer = (char*) malloc(BUFFER_SIZE * sizeof(char) + 1);
  int* result = NULL;
  int i = 0;
  int p = 0;
  int n = 0;
  int row = 0;
  int col = 0;

  while (getline(&buffer, &buffer_size, file) != -1) {
    switch (i) {
      case 0:
        sscanf(buffer, "%d", &p);
        break;
      case 1:
        sscanf(buffer, "%d", &n);
        die_if(p > n, "Invalid input");
        result = (int*) calloc(n * n + 2, sizeof(int));
        result[0] = p;
        result[1] = n;
        break;
      default:
        sscanf(buffer, "%d %d", &row, &col);
        result[(row - 1) * n + col + 1] = 1;
        break;
    }
    i++;
  }

  /* GC */
  free(buffer);
  fclose(file);
  return result;
}

int* get_transitive_closure(int* input) {
  int p = input[0];
  int n = input[1];
  int i = 0;
  int k = 0;
  int process = 0;
  int* previous_result = (int*) malloc(n * n * sizeof(int));
  int* result = NULL;

  /* Init shared memory */
  key_t row_shm_key = ftok(".", ROW_SHM_KEY);
  key_t tmp_shm_key = ftok(".", TMP_SHM_KEY);
  int row_shm_id = shmget(row_shm_key, sizeof(int), IPC_CREAT | 0666);
  int tmp_shm_id = shmget(tmp_shm_key, n * n * sizeof(int), IPC_CREAT | 0666);
  
  /* Init semaphores
   * sem-0 will be used as mutex lock, sem-1 will be used for sync */
  key_t sem_key = ftok(".", SEM_KEY);
  int sem_id = semget(sem_key, 2, IPC_CREAT|0666);
  semun sem_args;

  unsigned short sem_array[2] = {1, 0};

  sem_args.array = sem_array;
  semctl(sem_id, 0, SETALL, sem_args);

  struct sembuf mutex_lock = {0, -1, 0};
  struct sembuf mutex_unlock = {0, 1, 0};
  struct sembuf child_done = {1, 1, 0};
  struct sembuf sync = {1, -p, 0};
  
  /* k = 0 */
  for (i = 0; i < n * n; i++) {
    if (i % (n + 1) == 0) {
      previous_result[i] = 1;
    }
    else {
      previous_result[i] = input[i + 2];
    }
  }

  k = 1;

  while (k <= n) {
    int* row_p = (int*) shmat(row_shm_id, NULL, 0);
    *row_p = 0;

    /* Initialize tmp space */
    int* tmp = (int*) shmat(tmp_shm_id, NULL, 0);
    memset(tmp, 0, n * n * sizeof(int));

    for (process = 0; process < p; process++) {
      int child_pid = fork();
      die_if(child_pid < 0, "Fork error.");

      if (child_pid == 0) {
        int* tmp = (int*) shmat(tmp_shm_id, NULL, 0);
        while(1) {

          /* Mutex lock */
          semop(sem_id, &mutex_lock, 1);

          int* row_p = (int*) shmat(row_shm_id, NULL, 0);
          int row = *row_p;
          *row_p += 1;
          shmdt(row_p);

          /* Mutex unlock */
          semop(sem_id, &mutex_unlock, 1);

          if (row < n) {
            /* Do the actual work */
            /* Since the temp result won't meet conflict, we don't need to mutex-lock it */

            int j = 0;
            for (j = 0; j < n; j++) {
              int value = previous_result[row * n + j] ||
                (previous_result[row * n + k - 1] &&
                 previous_result[(k - 1) * n + j]);
              tmp[row * n + j] = value;
            }

          }
          else {
            /* When every row is done */
            /* GC */
            shmdt(tmp);
            free(previous_result);
            /* Notify parent this child is done */
            semop(sem_id, &child_done, 1);
            return;
          }
        }
      }
    }

    /* When all children are done, then k + 1 until k = n */
    semop(sem_id, &sync, 1);

    memcpy(previous_result, tmp, n * n * sizeof(int));
    shmdt(tmp);
    k += 1;
  }

  result = (int*) malloc((n * n + 2)* sizeof(int));
  result[0] = p;
  result[1] = n;

  memcpy(result + 2, previous_result, n * n * sizeof(int));

  /* GC */
  semctl(sem_id, 0, IPC_RMID);
  shmctl(row_shm_id, IPC_RMID, 0);
  shmctl(tmp_shm_id, IPC_RMID, 0);
  free(previous_result);

  return result;
}

void output(int* adjacency_matrix, int time) {
  int n = adjacency_matrix[1];
  int i = 0;
  int j = 0;
  printf("%d\n%d\n", adjacency_matrix[0], n);
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if (adjacency_matrix[i * n + j + 2] == 1) {
        printf("%d %d\n", i + 1, j + 1);
      }
    }
  }
  printf("%d\n", time);
}